# HornSAT

A linear time [hornsat](https://en.wikipedia.org/wiki/Horn-satisfiability) solver to get me started with Clojure.

Input format:
```
<# test cases>
<# variables> ;; test case 1
<# clauses>
<# literals> <var> <value (1 for true, 0 for false)> <var> <value> ...
<# variables> ;; test case 2
...
```

## Usage

```sh
cat resources/in | tee /dev/tty | lein run
./gen.pl -s | tee /dev/tty | lein run
```
