#!/usr/bin/perl

use strict;
use warnings;
use Getopt::Std;
use List::Util qw(min max sum);
use feature qw(say);

sub help {
    say "usage: perl gen.pl [options]";
    say "options:";
    say "  -c <int>     number of test cases (default is 2)";
    say "  -n <int>     number of variables (default is 5)";
    say "  -m <int>     number of clauses (default is 3)";
    say "  -t <int>     number of extra literals, in addition to -m (default is 2)";
    say "  -s           if present, -n, -m, -t are used as constant, else they are used as max";
    say "  -h           shows this";
    exit;
}

sub nextCase {
    my ($n, $m, $T) = @_;
    my @out = ($n, $m);
    my @ts = (1) x $m;
    $ts[int(rand($m))]++ foreach (1 .. $T);

    foreach (@ts) {
        my %line = ();
        my $t = min($n, $_);
        my @colors = (1 .. $n);
        foreach (1 .. $t) {
            my $offset = $n - keys %line;
            my $index = int(rand($offset));
            my $color = $colors[$index];
            @colors[$index, $offset - 1] = @colors[$offset - 1, $index];
            $line{$color} = sum values %line < 1 ? int(rand(2)) : 0;
        }

        @out = (@out, join(" ", ($t, %line)));
    }

    return @out;
}

my %o = ('c', 2, 'n', 5, 'm', 3, 't', 2);
getopts("c:n:m:t:sh", \%o);
help() if $o{'h'};
say $o{'c'};
map {say} nextCase(map {$o{'s'} ? $_ : max(1, int(rand($_)))} @o{'n', 'm', 't'}) foreach (1 .. $o{'c'});

