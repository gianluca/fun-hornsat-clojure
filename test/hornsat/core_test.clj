(ns hornsat.core-test
  (:require [clojure.test :refer :all]
            [hornsat.core :refer :all]))

(deftest all-test
  (let [args '((5) (3) (1 1 1) (2 1 0 2 0) (1 5 0) (1) (2) (1 1 0) (1 1 1))]
    (is (= '((1 0 0 0 0) nil) (all args)))))

