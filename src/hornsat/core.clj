(ns hornsat.core
  (:require [clojure.string :as s])
  (:gen-class))

(defn negvar [clause] (filter (fn [[v value]] (zero? value)) clause))

(defn posvar [clause] (filter (fn [[v value]] (pos? value)) clause))

(defn one [n clauses]
  ; clauses by neg var
  (let [cbnv (into {} (map (fn [v] (vector v (vec (filter
                                                    #(contains? % [v 0])
                                                    clauses))))
                           (range 1 (inc n))))]
    (loop [solution #{}
           ; scores by clauses
           sbc (into {} (map #(vector % (count (negvar %))) clauses))
           ; clauses by scores
           cbs (group-by #(count (negvar %)) clauses)]
      (if (and (contains? cbs 0) (seq (get cbs 0)))
        (let [curr (first (get cbs 0))
              nextsbc (dissoc sbc curr)
              nextcbs (assoc cbs 0 (vec (remove #{curr} (get cbs 0))))]
          (let [[head & tail] (posvar curr)]
            (cond
              (seq tail) (throw (IllegalArgumentException. "not a horn clause"))
              (not head) nil
              :else
                (let [[v _] head]
                  (cond
                    (contains? solution v)
                    (recur solution nextsbc nextcbs)
                    (contains? (get cbnv v) curr)
                    (recur solution nextsbc nextcbs)
                    :else
                    (recur
                      (conj solution v)
                      (into {} (for [[a b] nextsbc] [a (if (contains? (get cbnv v) a) (dec b) b)]))
                      (reduce (fn [out loser]
                                (let [score (get sbc loser)
                                      tmp (assoc out score (vec (remove #{loser} (get out score))))]
                                  (assoc tmp (dec score) (conj (or (get tmp (dec score)) []) loser))))
                              nextcbs
                              (get cbnv v))))))))
        (map #(if (contains? solution %) 1 0) (range 1 (inc n)))))))

(defn prepare [lines]
  (map (fn [x] (map #(Integer/parseInt %) x))
    (map #(s/split % #" ")
      (drop 1 lines))))

(defn all
  ([[[n] [m] & tail]]
    (if (and n m)
      (cons (one n (map set (map #(partition 2 (drop 1 %)) (take m tail))))
            (all (drop m tail)))
      '())))

(defn -main [& args]
  (if (> (.available System/in) 0)
      (dorun (map-indexed
        (fn [i line]
          (printf "Case #%d: %s\n"
                  (inc i)
                  (if line (s/join " " line) "IMPOSSIBLE")))
        (all (prepare (s/split-lines (slurp *in*))))))
      (println "no input")))

